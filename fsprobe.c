/* fsprobe.c

Authors:
  Olof Barring <Olof.Barring@cern.ch>
  Peter Kelemen <Peter.Kelemen@cern.ch>
  Tim Bell <Tim.Bell@cern.ch>

Copyright (c) CERN 2007
All rights not expressly granted under this license are reserved. 

Installation, use, reproduction, display, modification and
redistribution of this software, with or without modification,
in source and binary forms, are permitted on a non- exclusive
basis. Any exercise of rights by you under this license is subject
to the following conditions:

1. Redistributions of this software, in whole or in part, with
or without modification, must reproduce the above copyright
notice and these license conditions in this software, the
user documentation and any other materials provided with the
redistributed software.

2. The user documentation, if any, included with a redistribution,
must include the following notice: "This product includes software
developed by CERN"

If that is where third-party acknowledgments normally appear, this
acknowledgment must be reproduced in the modified version of this
software itself.

3. The names "CERN" and "European Organization for Nuclear
Research" may not be used to endorse or promote software, or
products derived therefrom, except with prior written permission
by CERN. If this software is redistributed in modified form,
the name and reference of the modified version must be clearly
distinguishable from that of this software.

4. You are under no obligation to provide anyone with any
modifications of this software that you may develop, including but
not limited to bug fixes, patches, upgrades or other enhancements
or derivatives of the features, functionality or performance
of this software. However, if you publish or distribute your
modifications without contemporaneously requiring users to enter
into a separate written license agreement, then you are deemed
to have granted CERN a license to your modifications, including
modifications protected by any patent owned by you, under the
conditions of this license.

5. You may not include this software in whole or in part in any
patent or patent application in respect of any modification of
this software developed by you.

6. DISCLAIMER

THIS SOFTWARE IS PROVIDED BY CERN "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, IMPLIED
WARRANTIES OF MERCHANTABILITY, OF SATISFACTORY QUALITY, AND
FITNESS FOR A PARTICULAR PURPOSE OR USE ARE DISCLAIMED. CERN MAKES
NO REPRESENTATION THAT THE SOFTWARE AND MODIFICATIONS THEREOF,
WILL NOT INFRINGE ANY PATENT, COPYRIGHT, TRADE SECRET OR OTHER
PROPRIETARY RIGHT.

7. LIMITATION OF LIABILITY

CERN SHALL HAVE NO LIABILITY FOR DIRECT, INDIRECT, SPECIAL,
INCIDENTAL, CONSEQUENTIAL, EXEMPLARY, OR PUNITIVE DAMAGES OF ANY
CHARACTER INCLUDING, WITHOUT LIMITATION, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES, LOSS OF USE, DATA OR PROFITS, OR BUSINESS
INTERRUPTION, HOWEVER CAUSED AND ON ANY THEORY OF CONTRACT,
WARRANTY, TORT (INCLUDING NEGLIGENCE), PRODUCT LIABILITY OR
OTHERWISE, ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

8. This license shall terminate with immediate effect and without
notice if you fail to comply with any of the terms of this
license, or if you institute litigation against CERN with regard
to this software.

*/

/* @(#)$RCSfile: fsprobe.c,v $ $Revision: 1.9 $ $Release$ $Date: 2007/06/24 07:42:41 $ $Author: fuji $ */

#define _GNU_SOURCE

#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <syslog.h>
#include <getopt.h>
#include <signal.h>

extern char *optarg;
extern int optind, opterr, optopt;

unsigned char *buffer = NULL;
unsigned char *readBuffer = NULL;
char *directoryName = NULL;
char *pathName = NULL;
char *mailTo = NULL;
char *mailSubject = "corruption";
char *logFileName = "/tmp/fsprobe.log";
size_t cycle, dumpCount;
size_t bufferSize = 1024*1024;
off64_t fileSize = (2*((off64_t)1024*1024*1024));
off64_t globalBufCount = 0;
off64_t bufCountStartPerFile;
int sleepTime = 3600;
int sleepBetweenBuffers = 1;
int runInForeground = 0;
int useRndBuf = 0;
int useSyslog = 0;
size_t nbLoops = 0;
int help_flag = 0;
int dumpBuffers = 0;
int continueOnDiff = 0;
int rndWait = 0;
int useSync = 0;
int forceCacheFlush = 0;
int useMultiPattern = 0;
int useBufCount = 0;
int useDirectIO = 0;

#define MAXTIMEBUFLEN 128
char timebuf[MAXTIMEBUFLEN];
size_t timebuflen;
time_t now;
struct tm timestamp;

#define BUFCNTLEN	(sizeof(globalBufCount))

const enum RunOptions
{
	Noop,
	PathName,
	LogFile,
	BufferSize,
	FileSize,
	NbLoops,
	SleepTime,
	IOSleepTime,
	Foreground,
	RndBuf,				/* Use buffers w/random data	*/
	MailTo,				/* Notify who?			*/
	MailSubject,			/* Subject of mail              */
	Syslog,				/* Mark corruption in syslog	*/
	DumpBuffers,			/* Dump both buffers on diff	*/
	ContinueOnDiff,			/* Continue checking until EOF	*/
	RndWait,			/* Wait random 0-30s at startup	*/
	Sync,				/* Use fdatasync()/fsync()	*/
	ForceCacheFlush,		/* malloc() how many megabytes	*/
	MultiPattern,			/* use more than 2 bit patterns */
	BufCount,			/* use buffer counters		*/
	DirectIO			/* O_DIRECT			*/
} runOptions;

const struct option longopts[] = 
{
	{"help",no_argument,&help_flag,'h'},
	{"PathName",required_argument,NULL,PathName},
	{"LogFile",required_argument,NULL,LogFile},
	{"BufferSize",required_argument,NULL,BufferSize},
	{"FileSize",required_argument,NULL,FileSize},
	{"NbLoops",required_argument,NULL,NbLoops},
	{"SleepTime",required_argument,NULL,SleepTime},
	{"IOSleepTime",required_argument,NULL,IOSleepTime},
	{"Foreground",no_argument,&runInForeground,Foreground},
	{"RndBuf",no_argument,&useRndBuf,RndBuf},
	{"MailTo",required_argument,NULL,MailTo},
	{"MailSubject",required_argument,NULL,MailSubject},
	{"Syslog",no_argument,&useSyslog,Syslog},
	{"DumpBuffers",no_argument,&dumpBuffers,DumpBuffers},
	{"ContinueOnDiff",no_argument,&continueOnDiff,ContinueOnDiff},
	{"RndWait",no_argument,&rndWait,RndWait},
	{"Sync",no_argument,&useSync,Sync},
	{"ForceCacheFlush",required_argument,NULL,ForceCacheFlush},
	{"MultiPattern",no_argument,&useMultiPattern,MultiPattern},
	{"BufCount",no_argument,&useBufCount,BufCount},
	{"DirectIO",no_argument,&useDirectIO,DirectIO},
	{NULL, 0, NULL, 0}
};

void usage(char *cmd)
{
	int i;
	
	fprintf(stdout,"Usage: %s \n",cmd);
	for (i=0; longopts[i].name != NULL; i++) {
		fprintf(stdout,"\t--%s %s\n",longopts[i].name,
		(longopts[i].has_arg == no_argument ? "" : longopts[i].name));
	}
	return;
}

void prepareTimeStamp(void)
{
	now = time(NULL);
	if ( localtime_r(&now, &timestamp) ) {
		timebuflen = strftime(timebuf, MAXTIMEBUFLEN, "%F %T ", &timestamp);
	} else {
		timebuflen = strlen(strcpy(timebuf, "localtime() NULL??? "));
	}
}

void myLog(char *str) 
{
	int fd, dontClose = 0;
	ssize_t rc;
	
	if ( str == NULL ) return;
	prepareTimeStamp();
	if ( (runInForeground != 0) && (strcmp(logFileName,"stderr") == 0) ) {
		dontClose = 1;
		fd = 2;
	} else {
		fd = open(logFileName,O_WRONLY|O_CREAT|O_APPEND,0644);
	}

	if ( fd == -1 ) {
		fprintf(stderr, "%s %s", timebuf, str);
		return;
	}
	rc=write(fd, timebuf, timebuflen);
	if (rc<0) {
	    perror("write timestamp to log");
	    exit(1);
	}
	rc=write(fd,str,strlen(str));
	if (rc<0) {
	    perror("write string to log");
	    exit(1);
	}
	if ( dontClose == 0 ) close(fd);
	return;
}

void sig_handler(int sig) 
{
    char buffer[256];
    sprintf(buffer,"Signal %s (%d) received\n",strsignal(sig),sig);
    myLog(buffer);
    exit(EINTR);
}

int initBuffers()
{
	int fdRandom = -1, i, randSize, rcRandom;
	
	if ( useDirectIO ) {
		i = posix_memalign((void *)&buffer, getpagesize(), bufferSize);
		if ( i ) {
			fprintf(stderr, "posix_memalign(%zu): error %d\n", bufferSize, i);
			return (-1);
		}
		
		i = posix_memalign((void *)&readBuffer, getpagesize(), bufferSize);
		if ( i ) {
			fprintf(stderr, "posix_memalign(%zu): error %d\n", bufferSize, i);
			return (-1);
		}
	} else {
		buffer = (unsigned char *)malloc(bufferSize);
		if ( buffer == NULL ) {
			fprintf(stderr,"Cannot initialize buffer: malloc(%zu) -> %s\n",bufferSize,
					    strerror(errno));
			return(-1);
		}
		readBuffer = (unsigned char *)malloc(bufferSize);
		if ( readBuffer == NULL ) {
			fprintf(stderr,"Cannot initialize buffer: malloc(%zu) -> %s\n",bufferSize,
					    strerror(errno));
			return(-1);
		}
	}

	if ( useRndBuf ) {
		fdRandom = open("/dev/urandom",O_RDONLY,0644);
		if ( fdRandom < 0 ) {
			fprintf(stderr,"open(/dev/urandom): %s\n",strerror(errno));
			return(-1);
		}
		i = randSize = 0;
		
		while ( randSize < bufferSize ) {
			rcRandom = read(fdRandom,buffer+i*512,512);
			if ( rcRandom < 0 ) {
				fprintf(stderr,"read(): %s\n",strerror(errno));
				return(-1);
			}
			i++;
			randSize += 512;
		}
		close(fdRandom);
	}
	
	return(0);
}

int putInBackground() 
{
	pid_t pid;
	int fdnull = -1, i, maxfds;
	char logbuf[2048];

	if ( (pid = fork()) < 0 ) {
		fprintf(stderr,"failed to go to background, fork(): %s\n",strerror(errno));
		return(-1);
	} else {
		if ( pid > 0 ) {
			printf("starting in background, pid=%d...\n",(int)pid);
			exit(0);
		}
		fdnull = open("/dev/null",O_RDWR);
		maxfds = getdtablesize();
		setsid();
		close(0);
		dup2(fdnull,1);
		dup2(fdnull,2);
		for ( i=3; i<maxfds; i++ ) {
			if ( i != fdnull ) close(i);
		}
	}
	sprintf(logbuf, "fsprobe v0.1 $Revision: 1.9 $ operational.\n");
	myLog(logbuf);

	sprintf(logbuf, "filesize %llu bufsize %zu sleeptime %u iosleeptime %u loops %zu\n",
		fileSize, bufferSize, sleepTime, sleepBetweenBuffers, nbLoops);
	myLog(logbuf);

	sprintf(logbuf, "rndbuf %u syslog %u dumpbuf %u cont %u rndwait %u sync %u flush %u\n",
		useRndBuf, useSyslog, dumpBuffers, continueOnDiff, rndWait, useSync, forceCacheFlush);
	myLog(logbuf);

	sprintf(logbuf, "multi %u bufcnt %u directio %u\n",
		useMultiPattern, useBufCount, useDirectIO);
	myLog(logbuf);
	signal(SIGINT,sig_handler);
	signal(SIGTERM,sig_handler);
	return(0);
}

int writeFile() 
{
	int fd, rc, flags;
	size_t j, bytesToWrite;
	off64_t bytesWritten;
	off64_t *bc;
	char logbuf[2048];
	
	flags = O_WRONLY|O_TRUNC|O_CREAT;
	if ( useDirectIO ) {
		flags |= O_DIRECT;
	}
	fd = open64(pathName, flags, 0644);
	if ( fd == -1 ) {
		sprintf(logbuf,"open(%s, %d) for write: %s\n",
			pathName, flags, strerror(errno));
		myLog(logbuf);
		return(-1);
	}

	bc = (off64_t *)buffer;

	bytesWritten = 0ULL;
	while ( bytesWritten < fileSize ) {
		if ( sleepBetweenBuffers ) {
			sleep(sleepBetweenBuffers);
		}
		bytesToWrite = bufferSize;
		if ( fileSize < bytesWritten+bufferSize ) {
			bytesToWrite = (size_t)(fileSize-bytesWritten);
			sprintf(logbuf, "tail write: cycle %zu bufcnt %llu bytes %zu\n",
				cycle, globalBufCount, bytesToWrite);
			myLog(logbuf);
		}
		j = 0;
		if ( useBufCount ) *bc = globalBufCount;
		while ( j < bytesToWrite ) {
			rc = write(fd, buffer+j, bytesToWrite-j);
			if ( rc == -1 ) {
				sprintf(logbuf,"write(%s): %s\n",pathName,strerror(errno));
				myLog(logbuf);
				close(fd);
				return(-1);
			}
			if ( rc != bytesToWrite-j ) {
				sprintf(logbuf, "partial write: cycle %zu bufcnt %llu bufpos %zu offset %llu req %zu got %u\n",
					cycle, globalBufCount, j, bytesWritten+j, bytesToWrite-j, rc);
				myLog(logbuf);
			}
			j += rc;
			if ( useSync ) {
				rc = fdatasync(fd);
				if ( rc == -1 ) {
					sprintf(logbuf,"fdatasync(%s): %s\n",pathName,strerror(errno));
					myLog(logbuf);
					close(fd);
					return(-1);
				}
			}
		}
		bytesWritten += bytesToWrite;
		globalBufCount++;
	}
	if ( useSync ) {
		rc = fsync(fd);
		if ( rc == -1 ) {
			sprintf(logbuf,"fsync(%s): %s\n",pathName,strerror(errno));
			myLog(logbuf);
			close(fd);
			return(-1);
		}
	}
	rc = close(fd);
	if ( rc == -1 ) {
		sprintf(logbuf,"close(%s): %s\n",pathName,strerror(errno));
		myLog(logbuf);
		return(-1);
	}
	return(0);
}

/* NOTE(fuji): There is no guarantee that read() will not read cached data,
 * i.e. corruptions on disk are not guaranteed to be exposed if the kernel
 * cache doesn't have a high turnover rate, IOW I/O load.  Currently, we
 * assume `sleepTime' number of seconds elapsed between write and read are
 * sufficiently long for the cache to change.
 */
int checkFile() 
{
	int fd, rc, flags, dumpfd;
	size_t i, j, bytesToRead, diffCount;
	off64_t bytesRead, bufCount;
	off64_t *bc;
	char logbuf[2048];
	char dumpPathName[1024];
	int diffFound = 0;
	int bufStart;
	
	flags = O_RDONLY;
	if ( useDirectIO ) {
		flags |= O_DIRECT;
	}
	fd = open64(pathName, flags, 0644);
	if ( fd == -1 ) {
		sprintf(logbuf,"open(%s, %d) for read: %s\n",
			pathName, flags, strerror(errno));
		myLog(logbuf);
		return(-1);
	}

	bytesRead = 0ULL;
	bufCount = bufCountStartPerFile;
	while ( bytesRead < fileSize ) {
		if ( sleepBetweenBuffers ) {
			sleep(sleepBetweenBuffers);
		}
		
		bytesToRead = bufferSize;
		if ( fileSize < bytesRead+bufferSize ) {
			bytesToRead = (size_t)(fileSize-bytesRead);
			sprintf(logbuf, "tail read: cycle %zu bufcnt %llu bytes %zu\n",
				cycle, bufCount, bytesToRead);
			myLog(logbuf);
		}
		j = 0;
		while ( j < bytesToRead ) {
			rc = read(fd, readBuffer+j, bytesToRead-j);
			if ( rc == -1 ) {
				sprintf(logbuf,"read(%s): %s\n",pathName,strerror(errno));
				myLog(logbuf);
				close(fd);
				return(-1);
			}
			if ( rc != bytesToRead-j ) {
				sprintf(logbuf, "partial read: cycle %zu bufcnt %llu bufpos %zu offset %llu req %zu got %u\n",
					cycle, bufCount, j, bytesRead+j, bytesToRead-j, rc);
				myLog(logbuf);
			}
			j += rc;
		}

		diffCount = 0;
		if ( useBufCount ) {
			bc = (off64_t *)readBuffer;
			if ( bufCount != *bc ) {
				sprintf(logbuf, "cntdiff: cycle %u expected %llu (0x%08llX) got %llu (0x%08llX)\n",
					cycle, bufCount, bufCount, *bc, *bc);
				myLog(logbuf);
				for (i = 0; i < BUFCNTLEN; i++) {
					if ( *(buffer+i) == *(readBuffer+i) ) {
						continue;
					}
					diffCount++;
				}
			}
			bufStart = BUFCNTLEN;	/* compare pattern bytes after the counter */
		} else {
			bufStart = 0;
		}
		for (i = bufStart; i < bytesToRead; i++) {
			if ( *(buffer+i) == *(readBuffer+i) ) {
				continue;
			}
			diffCount++;
			/* TB 15/03/07 do not fill up /var when lots */
			/* of errors */
			if (diffCount<64) {
			    sprintf(logbuf, "diff: cycle %zu bufcnt %llu bufpos %08X %zu offset %08llX %llu expected 0x%02X got 0x%02X\n",
				cycle,
				bufCount,
				i,
				i,
				bytesRead+i, 
				bytesRead+i, 
				*(buffer+i), 
				*(readBuffer+i) );
			    myLog(logbuf);
			}
		}
		if ( diffCount ) {
			sprintf(logbuf, "total %zu differing bytes found\n", diffCount);
			myLog(logbuf);
			diffFound++;
		}

		rc = memcmp(buffer+bufStart, readBuffer+bufStart, bytesToRead-bufStart);
		if ( rc != 0 ) {
			sprintf(logbuf,"Corruption found in %s after %llu bytes\n",
				      pathName,bytesRead);
			myLog(logbuf);
		}

		/* NOTE(fuji): Only do accounting here so that offsets are
		 * correct in any messages logged in above code. */
		bytesRead += bytesToRead;
		bufCount++;

		if ( rc == 0 && diffCount != 0 ) {
			sprintf(logbuf, "OUCH, memcmp() missed some differences!\n");
			myLog(logbuf);
		}
		if ( rc != 0 && diffCount == 0 ) {
			sprintf(logbuf, "OUCH, only memcmp() thinks there are differences!\n");
			myLog(logbuf);
		}

		if ( rc != 0 || diffCount != 0 ) {
			if ( useSyslog != 0 ) {
				syslog(LOG_ALERT,"fsprobe %s",logbuf);
			}
			if ( mailTo != NULL ) {
				sprintf(logbuf,
					"tail %s |"
					"mail -s \"%s\" %s",
				        logFileName, mailSubject, mailTo);
				myLog(logbuf);
				system(logbuf);
			}
			if ( dumpBuffers ) {
				dumpCount++;
				sprintf(dumpPathName, "%s.%zu.%zu.ob", pathName, cycle, dumpCount);
				dumpfd = open(dumpPathName, O_WRONLY|O_TRUNC|O_CREAT, 0644);
				if ( dumpfd ) {
					write(dumpfd, buffer, bufferSize);
					close(dumpfd);
				} else {
					sprintf(logbuf, "dump failed: %s, error %d (%s)\n",
						dumpPathName, errno, strerror(errno));
					myLog(logbuf);
				}
				sprintf(dumpPathName, "%s.%zu.%zu.rb", pathName, cycle, dumpCount);
				dumpfd = open(dumpPathName, O_WRONLY|O_TRUNC|O_CREAT, 0644);
				if ( dumpfd ) {
					write(dumpfd, readBuffer, bufferSize);
					close(dumpfd);
				} else {
					sprintf(logbuf, "dump failed: %s, error %d (%s)\n",
						dumpPathName, errno, strerror(errno));
					myLog(logbuf);
				}
			}
			if ( !continueOnDiff ) break;
		}

	}
	rc = close(fd);
	if ( rc == -1 ) {
		sprintf(logbuf,"close(%s): %s\n",pathName,strerror(errno));
		myLog(logbuf);
		if ( diffFound ) {
			return (-2);
		}
		return(-1);
	}
	if ( diffFound ) {
		return (-2);
	}
	return(0);
}

#define MAXPATTERN 6

void
prepareBitPatternBuffer(void)
{
	unsigned char patterns[MAXPATTERN] = { 0x55, 0xAA, 0x33, 0xCC, 0x0F, 0xF0 };

	if ( useMultiPattern ) {
		memset(buffer, patterns[cycle % MAXPATTERN], bufferSize);
	} else {
		if ( cycle & 1UL ) {
			memset(buffer, 0xAA, bufferSize);
		} else {
			memset(buffer, 0x55, bufferSize);
		}
	}
}

int main(int argc, char *argv[]) 
{
	struct stat64 st;
	int rc;
	char corruptPathName[1024];
	char *cmd, ch;
	void *cacheflush;
	static char logbuf[1024];

	optind = 1;
	opterr = 1;
	cmd = argv[0];
	while ((ch = getopt_long(argc,argv,"h",longopts,NULL)) != EOF) {
		switch (ch) {
			case PathName:
				pathName = strdup(optarg);
				break;
			case LogFile:
				logFileName = strdup(optarg);
				break;
			case BufferSize:
				bufferSize = atoi(optarg);
				break;
			case FileSize:
				fileSize = atoll(optarg);
				break;
			case NbLoops:
				nbLoops = atoi(optarg);
				break;
			case SleepTime:
				sleepTime = atoi(optarg);
				break;
			case IOSleepTime:
				sleepBetweenBuffers = atoi(optarg);
				break;
			case MailTo:
				mailTo = strdup(optarg);
				break;
			case MailSubject:
				mailSubject = strdup(optarg);
				break;
			case ForceCacheFlush:
				forceCacheFlush = atoi(optarg);
				break;
			case 'h':
				usage(cmd);
			default:
				break;
		}
	}
	if ( pathName == NULL ) {
		fprintf(stderr,"Please provide a directory name with --PathName!!!\n");
		usage(cmd);
		exit(1);
	}

	if ( bufferSize < 2*BUFCNTLEN ) {
		fprintf(stderr, "BufferSize must be larger than %zu bytes!\n",
			2*BUFCNTLEN);
		exit(1);
	}
	if ( fileSize % bufferSize != 0 ) {
		if ( fileSize % bufferSize < 2*BUFCNTLEN ) {
			fprintf(stderr, "FileSize(%llu) %% BufferSize(%zu) < 2*%zu\n",
				fileSize, bufferSize, BUFCNTLEN);
			exit(1);
		}
	}

	if ( useDirectIO ) {
		if ( (bufferSize % getpagesize()) != 0 ) {
			fprintf(stderr, "BufferSize(%zu) must be multiple of page size (%u) for direct I/O!\n",
				bufferSize, getpagesize() );
			exit(1);
		}
		if ( (fileSize % getpagesize()) != 0 ) {
			fprintf(stderr, "FileSize(%llu) must be multiple of page size (%u) for direct I/O!\n",
				fileSize, getpagesize() );
			exit(1);
		}
	}

	/*
	 * Check that we can use the provided path
	 */
	rc = stat64(pathName,&st);
	if ( rc == 0 ) {
		if ( !S_ISREG(st.st_mode) ) {
			fprintf(stderr,"Cannot use path %s, not a regular filename\n",pathName);
			exit(1);
		}
	}
	rc = open64(pathName,O_WRONLY|O_CREAT,0644);
	if ( rc == -1 ) {
		fprintf(stderr,"Error opening path %s: %s\n",pathName,strerror(errno));
		exit(1);
	}
	close(rc);
	
	rc = unlink(pathName);
	if ( rc == -1 ) {
		fprintf(stderr,"Error removing path %s: %s\n",pathName,strerror(errno));
		exit(1);
	}

	/*
	 * Path is OK. Initialize...
	 */
	rc = initBuffers();
	if ( rc == -1 ) exit(1);


	if ( runInForeground == 0 ) {
		rc = putInBackground();
		if ( rc == -1 ) exit(1);
	}

	if ( rndWait ) {
		srandom(1);
		sleep(random() % 30);
	}

	dumpCount = 0;
	cycle = 0;
	while ( (nbLoops == 0) || (cycle < nbLoops) ) {
		if ( ! useRndBuf ) {
			prepareBitPatternBuffer();
		}
		bufCountStartPerFile = globalBufCount;
		rc = writeFile();
		if ( rc == 0 ) {
			rc = checkFile();
			if ( rc == -2 ) {      /* diff found */
				sprintf(corruptPathName,"%s.%zu",pathName,cycle);
				(void)rename(pathName,corruptPathName);
			}
			if (rc==-1) {
			    sprintf(logbuf, "cannot check files cycle=%zd\n",cycle);
			    myLog(logbuf);
			    exit(2);
			}
		} else {
			sprintf(logbuf, "cannot write files cycle=%zd\n",cycle);
			myLog(logbuf);
			exit(2);
		}
		    
		(void)unlink(pathName);
		if ( forceCacheFlush ) {
			cacheflush = malloc(forceCacheFlush*1024UL*1024);
			if ( cacheflush ) {
				memset(cacheflush, 0xFF, forceCacheFlush*1024UL*1024);
				free(cacheflush);
			} else {
				perror("malloc");
			}
		}
		if ( sleepTime ) {
			sleep(sleepTime);
		}
		cycle++;
	}
	exit(0);
}

/* vim: set number nowrap: */
/* End of file. */
