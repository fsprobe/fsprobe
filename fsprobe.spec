# $Id: fsprobe.spec,v 1.7 2007/06/24 07:42:41 fuji Exp $

Summary: CERN probabilistic data integrity checker
Name: fsprobe
Version: 0.1
Release: 2
Group: CERN/Utilities
Buildroot: %{_tmppath}/%{name}-%{version}-root
Source: $RPM_SOURCE_DIR/%{name}-%{version}.tar.gz
Packager: Peter.Kelemen@cern.ch
License: CERN
Vendor: CERN
BuildRequires: coreutils, rpm, tar
Requires: coreutils, findutils, initscripts, mailx, procps, util-linux
Conflicts: CERN-CC-fsprobe

%description
CERN probabilistic data integrity checker

%prep
%setup -q

%build
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir $RPM_BUILD_ROOT
cd $RPM_BUILD_ROOT

mkdir -p etc/init.d etc/logrotate.d etc/cron.daily etc/sysconfig usr/sbin
install -m 755 $RPM_BUILD_DIR/%{name}-%{version}/rc.fsprobe etc/init.d/fsprobe
install -m 755 $RPM_BUILD_DIR/%{name}-%{version}/cron.fsprobe etc/cron.daily/fsprobe
install -m 644 $RPM_BUILD_DIR/%{name}-%{version}/logrotate.fsprobe etc/logrotate.d/fsprobe
install -m 644 $RPM_BUILD_DIR/%{name}-%{version}/sysconfig.fsprobe etc/sysconfig/fsprobe
install -m 755 $RPM_BUILD_DIR/%{name}-%{version}/fsprobe usr/sbin/fsprobe

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/etc/init.d/fsprobe
/etc/logrotate.d/fsprobe
/etc/cron.daily/fsprobe
/etc/sysconfig/fsprobe
/usr/sbin/fsprobe

%post
mkdir -p /tmp/fsprobe
/sbin/service fsprobe restart

%changelog
* Sat Jun 23 2007 KELEMEN Peter <Peter.Kelemen@cern.ch> 0.1-2
- fix source package, revamp build

* Sat Jun 23 2007 KELEMEN Peter <Peter.Kelemen@cern.ch> 0.1-1
- rename package, proper versioning, default config, /opt -> /usr/sbin
