# $Id: Makefile,v 1.4 2007/06/24 07:42:40 fuji Exp $

VERSION = 0.1
SPECFILE = fsprobe.spec
FILES = fsprobe.c Makefile $(SPECFILE) rc.fsprobe logrotate.fsprobe cron.fsprobe sysconfig.fsprobe

rpmtopdir := $(shell rpm --eval %_topdir)
rpmbuild  := $(shell [ -x /usr/bin/rpmbuild ] && echo rpmbuild || echo rpm)

PKGNAME = $(shell grep -s '^Name:' $(SPECFILE) | sed -e 's/Name: //')
PKGVERS = $(shell grep -s '^Version:' $(SPECFILE) | sed -e 's/Version: //')
DISTDIR = $(PKGNAME)
DISTTGZ = $(PKGNAME)-$(VERSION).tar.gz

RPMARCH=noarch
RPMSARCH=noarch
SWREPARCH=i386

ifeq ($(arch),ia64)
RPMARCH=ia64
RPMSARCH=ia64
endif

ifeq ($(arch),x86_64)
RPMARCH=amd64
RPMSARCH=x86_64
endif

ifeq ($(uname -r | cut -d. -f1,2),2.6)
OSVER=slc4
else
OSVER=slc3
endif

CFLAGS=-g -O0 -Wall -ansi
fsprobe:	fsprobe.c
	$(CC) -o $@ $(CFLAGS) fsprobe.c

tar: clean
	mkdir fsprobe-$(VERSION)
	cp $(FILES) fsprobe-$(VERSION)
	tar cfz $(DISTTGZ) fsprobe-$(VERSION)

rpm: $(SPECFILE) always tar
	$(rpmbuild) -ta $(DISTTGZ)

clean:	always
	rm -f fsprobe fsprobe.o
	rm -rf fsprobe-$(VERSION)

always:

.PHONY: check codespell cppcheck

check: codespell cppcheck

codespell:
	codespell --enable-colors -L therefrom

cppcheck:
	cppcheck --error-exitcode=1 --quiet --verbose .
